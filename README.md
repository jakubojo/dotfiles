# .dotfiles

>```~/.*```

## Setup

```bash
git init --bare $HOME/.dotfiles

alias config="/usr/bin git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"

config config --local status.showUntrackedFiles no
```
