
###########
#  ZSHRC  #
###########


HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob
bindkey -v
zstyle :compinstall filename '$ZDOTDIR/.zshrc'

autoload -Uz compinit; compinit

# alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# +---------+
# | ALIASES |
# +---------+
[[ -f "$ZDOTDIR/alias.zsh" ]] \
    && source $ZDOTDIR/alias.zsh
