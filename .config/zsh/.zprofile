##############
#  ZPROFILE  #
##############

# +---------+
# | PROFILE |
# +---------+
[[ -f ~/.profile ]] \
    && source ~./.profile

# +-----------+
# | SSH-AGENT |
# +-----------+

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
