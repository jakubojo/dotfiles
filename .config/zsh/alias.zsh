#!/usr/bin bash

# +----+
# | ls |
# +----+

alias la='ls -A'

# +-----+
# | Git |
# +-----+
#
# override to interact with configuration repo
alias config='/usr/bin/git --git-dir=$DOTFILES --work-tree=$HOME'
